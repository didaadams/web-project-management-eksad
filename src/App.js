import React from 'react';
import logo from './logo.svg';
import './App.css';
import HomeRoutes from './routes/HomeRoutes'
import { BrowserRouter } from 'react-router-dom';
import '../src/config/ReactotronConfig'

function App() {
  return (
    <BrowserRouter>
      <HomeRoutes/>
    </BrowserRouter>
  );
}

export default App;
