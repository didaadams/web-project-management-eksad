const constant = {
    BASE_URL: {
        API: "http://localhost:8000/backend",
    },

    API: {
        SUCCESS: "success",
        ERROR: "error",
    },
};

export default constant;
