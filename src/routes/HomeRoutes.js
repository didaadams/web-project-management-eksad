import React from 'react';
import { Switch, Route } from 'react-router-dom';

// import PrivateRouter from './PrivateRouter';

// import page screen
import Login from '../containers/LoginScreen';
import HomeScreen from '../containers/layout/HomeSreen'

// import AppContainer from '../containers/layout/AppContainer';

const AppRouter = () => {
    return (
        <main>
            <Switch>
                <Route path='/login' component={Login} />
                <Route path='/home' component={HomeScreen} />
                {/* <PrivateRouter component={AppContainer} /> */}
            </Switch>
        </main>
    )
}

export default AppRouter;    