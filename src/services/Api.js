import apisauce from 'apisauce'
import {Constant} from '../library'

const create = () => {

    const api = apisauce.create({
        baseURL: `${constant.BASE_URL}`,
        headers: {
        'Cache-Control': 'no-cache',
        },
        timeout: 30000
    })

    api.addAsyncRequestTransform(request => async () => {
        var token
        try {
        const res = await localStorage.getItem(Constant.TOKEN)
        if (res != null) {
            token = res
        } else {
            token = ""
        }
        } catch (error) {
        // console.tron.log(error)
        }
        request.headers['token'] = token
        // console.tron.log(url)
    })

    // inisialisasi
    const GET = api.get
    const POST = api.post

    const checkToken = (data) => POST('controller/check_token')

    return {
        checkToken
    }
}

export default {create};